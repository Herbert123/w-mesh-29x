# W_Mesh_29x
## Blender add-on for parametric objects
This add-on extends Blender 2.9 with additional parametric objects. These objects remain "live" in contrast to the standard primitives which, once created, cannot be adjusted afterwards. wPrimitives' parameters may be changed at any time. The properties can be animated.

### Installation
Updated 2.9x compatible plugin from https://github.com/WiresoulStudio/W_Mesh_28x/releases by Vit Prochazka.

To install: download the zip file. Open the preferences, and select the Add-ons tab. Click the Install button and point to the zip file. Activate.

### Usage
To add a parametric object: In Object mode choose Add, wPrimitives. Pick the object you want.

![Parameters](https://i.ibb.co/xsWTR1B/wmesh-01.png)

To change the live object's parameters, open the Object Data properties in the Properties panel. Navigate to and twirl down the wMesh data flydown. Adjust the parameters.

![Parameters](https://i.ibb.co/k54V5LV/wmesh-02.png)

To convert to a regular mesh, click the Convert to Regular Mesh button.